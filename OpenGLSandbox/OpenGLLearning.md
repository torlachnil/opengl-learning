# OpenGL Learning!

I am learning openGL and C++ from this playlist: https://www.youtube.com/watch?v=W3gAzLwfIP0&list=PLlrATfBNZ98foTJPJ_Ev03o2oq3-GGOS2

If you are interested, you should probably check this channel out. This is easily
some of the best teaching content I have ever seen.

### Section 1

OpenGL is  a standard for communicating with graphics cards (according to me, who works in Telecom).
To interface with OpenGL, one can consider many options. I use a combination of GLFW (https://www.glfw.org/)
and GLEW(http://glew.sourceforge.net/). 

OpenGL can be categorized into *old* openGL and *modern* openGL. Old openGL was "easy" to work with,
 but not very flexible. Modern openGL is much more versatile, but requires more work on the part of the user. 

With GLFW, we can in a simple manner interface with the window API of the OS, creating a so-called 
"OpenGL rendering context". There are then some library functions that for example let us draw a triangle.
 The following code snippet is largely taken from the glfw website. 

```C++
#include <GLFW\glfw3.h>

int main() {
   GLFWwindow* window;

   /*initialize library*/
   if (!glfwInit()) {
      return -1;
   }

   /* Create a windowed mode window and it's OpenGL context */
   window = glfwCreateWindow(640, 480, "Hello World", NULL, NULL);
   if (!window) {
      glfwTerminate();
      return -1;
   }

   /* Make the window's context current */
   glfwMakeContextCurrent(window);
   glewInit();

      /* While loop until window is closed */
      while (!glfwWindowShouldClose(window)) {
         /* Render here */
         glClear(GL_COLOR_BUFFER_BIT);

         glBegin(GL_TRIANGLES);
         glVertex2f(-0.5f, -0.5f);
         glVertex2f( 0.0f,  0.5f);
         glVertex2f( 0.5f, -0.5f);
         glEnd();

         /* Swap front and back buffers */
         glfwSwapBuffers(window);

         /* Poll for and process events */
         glfwPollEvents();
      }
   glfwTerminate();
   return 0;
```
Of course, this is assuming you have an environment where you can get this to link. I have 
opted for the quick & dirty route of just keeping the .lib and .h files for the libraries
of interest inside my working folder. Since I am statically linking, I have to #define GL_STATIC
in my environment (or as a -D flag to clang). The triangle comes out roughly like this:

![](./pictures/whiteTriangle.PNG)

In modern openGL, you will have to do significantly more work. Essentially, there are
two main components:

 * Getting information about the graphics you want to draw into the VRAM if your GPU
   * In GLEW, this is achieved by allocating a so-called **vertex buffer**, which is just a buffer of memory
    that happens to be located in VRAM. The way we do this is by first allocating a buffer on the stack and performing
    a "bind" operation - which puts this buffer in VRAM. We can then push data to this buffer. 
```C++ 
/* Initialize & populate Vertex buffer */
   float positions[] = {
      -0.5f, -0.5f,
       0.0f,  0.5f,
       0.5f, -0.5f
   };

   unsigned int buffer;
   glGenBuffers(1, &buffer);
   glBindBuffer(GL_ARRAY_BUFFER, buffer);
   glBufferData(GL_ARRAY_BUFFER, sizeof(positions), positions, GL_STATIC_DRAW);
   // The last argument here is a hint to the GL implementation that the buffer will not
   // often be written to, but it will be read often. I use docs.gl for documentation
``` 
 * Telling your graphics card what in the world this information means
   * The buffer you send to the graphics card is recieved as a void pointer, so the 
     GL has no clue in what way this data is to be interpreted.
   * This involves writing a "shader" - which is just code for the graphics card. 
   The graphics card can then interpret the buffer you have provided as long as
   you point out the appropriate shader. 
 * After having specified the shader and the buffer you can instruct the graphics card to
   actually draw your thing. 

Before we give any example code, we are goin to have to learn about shaders. Before this, a note 
about all functions from GLEW. 

### GLEW - an investigation

An interesting note to make is that if you try to follow the functions that you get access to
via the GLEW library, for example by ctrl+clicking the functions you are using in a modern IDE,
you will end up in a location like this:
```C++
#define glBufferData GLEW_GET_FUN(__glewBufferData)
```

If you look for the definition of GLEW_GET_FUN you just find

```C++
#ifndef GLEW_GET_FUN
    #define GLEW_GET_FUN(x) x
#endif
```

which is not very illuminating. So instead, let us try to follow the argument, __glewBufferData.
In this case we find the line
```C++
GLEW_FUN_EXPORT PFNGLBUFFERDATAPROC __glewBufferData;
```
Following GLEW_FUN_EXPORT we find
```C++
#define GLEW_FUN_EXPORT GLEWAPI
```
and GLEWAPI maps to
```C++
/*
 * GLEW_STATIC is defined for static library.
 * GLEW_BUILD  is defined for building the DLL library.
 */

#ifdef GLEW_STATIC
    #define GLEWAPI extern
#else
```
So, when statically linking to GLEW, it seems that this is just telling us to expect 
PFNGLBUFFERDATAPROC __glewBufferData to be defined elsewhere. In fact, the function 
is being declared here:
```C++
typedef void (GLAPIENTRY * PFNGLBUFFERDATAPROC) (GLenum target, GLsizeiptr size, const void* data, GLenum usage);
```
where GLAPIENTRY reads
```C++
#    ifndef GLAPIENTRY
#      define GLAPIENTRY __stdcall
```
Here, __stdcall is specifying that the function PFNGLBUFFERDATAPROC is called with the specific 
convention "standard call". This means that the responsibility for cleaning up the stack context
created by the function falls on the function be called, rather than the function making the call.

In the expression "* PFNGLBUFFERDATAPROC" we are dereferencing whatever adress is represented by the externally defined
constant PFNGLBUFFERDATAPROC. This is a function pointer that is defined by the drivers for
my GPU, for which I have no insight into the implementation. 


### Shaders

**GL vertex attribute pointers**

To help GL interpret the void pointer we send it, we use GL vertex attributes. The vertex
attribute pointer specifies the layout of the memory buffer we just gave to the graphics card.
The signature of this function is as follows:
```C++
void glVertexAttribPointer(	GLuint index,
    GLint size,
    GLenum type,
    GLboolean normalized,
    GLsizei stride,
    const GLvoid * pointer);
```
and the documentation is here: 
http://docs.gl/gl4/glVertexAttribPointer.

To understand this signature, let us establish some basic terminology. 
 * A vertex is all the features of one point on the geometry. These include for example:
   * The coordinates of the vertex
   * The unit normal at the vertex
   * Whatever other features you might come up with for the vertex
 * An attribute is one set of related values related to the vertex i.e. one "feature" such as "location"

With this terminology, we can go through the arguments:
* **GLuint index**
  * This argument points to the start of the feature of interest of some vertex. 
  * 0 refers to the first byte of the feature of interest within one vertex.
* **GLint size**
  * This argument refers to the size of the attribute, in our case of two points 
    specifying the location of each vertex, the size is two "elements". The size
    in bytes is understood from the next argument.
* **GLenum type**
  * Type specification of the data at the specified location.
  * example, GL_FLOAT. 
* **GLboolean normalized**
  * Specifies whether fixed-point data values should be normalized or not (GL_TRUE or GL_FALSE)
* **GLsizei stride**
  * The size in bytes id the set of all attributes related to a specific vertex. If the vertices
    are contiguous in memory, perhaps as a struct, this stride length specifies the distance between
    two attributes of the same type assosiacted to different vertices. 
* **const GLvoid\* pointer**
  * The offset relaitve to the first byte of the currently bound vertex buffer of the first byte
    of the first vertex. This is useful if you have put several different types of vertex or
    other information in the vertex buffer.

In our example, the call should look as in this snippet.
```C++
   /* Initialize & populate Vertex buffer */

   float positions[6] = {
      -0.5f, -0.5f,
       0.0f,  0.5f,
       0.5f, -0.5f
   };

   unsigned int buffer;
   glGenBuffers(1, &buffer);
   glBindBuffer(GL_ARRAY_BUFFER, buffer);
   glBufferData(GL_ARRAY_BUFFER, sizeof(positions), positions, GL_STATIC_DRAW);

   glEnableVertexAttribArray(0);
   glVertexAttribPointer(0, 2, GL_FLOAT, GL_FALSE, sizeof(float)*2, 0);
```
To specify the offset in the last argument in the nontrivial case, we simply do something like
```C++
    (void*) NONTRIVIAL_INTEGER
```

**Shaders**

A shader is written in text on our computer, and sent to openGL which will compile it to
machine code meant for the target GPU. In my case the "openGL" that I am using is part of
the NVIDIA 450.12 drivers. With these drivers, "glDrawArrays(GL_TRIANGLES, 0, 3);" works
right away, because the NVIDIA drivers provide a default shader when none has been specified.
This is not part of the openGL standard, so may not work on all computers. 

Two most popular shaders:
* Vertex shaders
* Fragment (or Pixel) shaders

Other types: tesselation, geometry shaders, compute shaders.

We will begin with focusing on vertex and fragment shaders. 

Shaders are called by the GPU in response to us issuing a "draw call", in this case
```C++
    glDrawArrays(GL_TRIANGLES, 0, 3);
```
This will start some kind of graphics pipeline, where the vertex shader is called before 
the fragment shader. There is a rasterization stage in there somewhere. We will learn more 
about this pipeline later, but it seems to mostly be an ordered sequence of shaders.

The purpose of the vertex shader is to tell the graphics card where in the image you want 
to draw something. It also passes data along to the later stager (for example attributes
beyond just positions). The way the vertex shader knows knows where the vertices are is because
we called glVertexAttribPointer. 

The purpose of the fragment shader is to color each pixel, determining the color based on the
known vertex attributes. 

The vertex shader is called 3 times in our current example. With the window we have (see this line)
```C++
   window = glfwCreateWindow(640, 480, "Hello World", NULL, NULL);
```
we have on the order on 250000 pixels, meaning 250000 calls to the fragment shader. When thinking about performance, it is clear that we 
want to compute as little as possible in the fragment shader. A typical thing that has to be
computed by the fragment shader is the precise color a pixel should have in the presence of 
lighting for surfaces with some specific reflective properties. In real applications, shaders
are often generated on the fly, based on the graphics settings of the user and context information
known by the game engine. 

To use a shader, you tell openGL to enable it. Data is sent to the shader in the form of a **uniform**.
This ties back to openGL being a state machine. The general workflow is to put openGL into the
state that will make it draw what you want when you issue a draw call. The following sequence
of functions will have to be called with some appropriate arguments. 

http://docs.gl/gl4/glCreateProgram - Creates an empty object for the GPU program you want to run. 
Returns an int that with which that program can be referenced. 

http://docs.gl/gl4/glCreateShader - Creates an empty shader of a desired type (vertex, 
fragment). Returns a reference ID as an int.

http://docs.gl/gl4/glShaderSource - Passes a string of source code to the shader object.

http://docs.gl/gl4/glCompileShader - Compiles the source code string of a shader object

http://docs.gl/gl4/glAttachShader - Attaches a shader to a program

http://docs.gl/gl4/glLinkProgram - Links the program (using the compiled code of any 
attached shaders)

At this point, the shaders can be deleted as they can be seen as intermediates (~obj files). 
This has very little memory upside and actually sabotages some debugging functionality,
so this is not commonly done.

http://docs.gl/gl4/glValidateProgram - Validates the program. 


http://docs.gl/gl4/glUseProgram - Instructs GPU to use program on next draw call.


I wrapped the raw opengl function calles in two layers. One function to compile a generic
 shader and handle compile errors as follows:
```C++
/*
IN: source              Source code for generic shader
IN: type                Type of shader (some #define from GLEW)
*/
static unsigned int CompileShader(const std::string& source, unsigned int type)
{
   unsigned int id = glCreateShader(type);
   const char* src = source.c_str(); // or &source[0]
   glShaderSource(id, 1, &src, NULL);
   glCompileShader(id);

   // TODO: Error handling
   int result;
   glGetShaderiv(id, GL_COMPILE_STATUS, &result);
   if (result == GL_FALSE)
   {
      int length;
      glGetShaderiv(id, GL_INFO_LOG_LENGTH, &length);
      // alloca is a tool for manually allocating on the stack so it gets cleaned on return
      char* message = (char*) alloca(length * sizeof(char));
      glGetShaderInfoLog(id, length, &length, message);
      std::cout << "Failed to compile " <<
         (type == GL_VERTEX_SHADER ? "vertex" : "fragment") << std::endl;
      std::cout << message << std::endl;
      glDeleteShader(id);
      return 0;
   }
   return id;
}
```
The compile function is called by a function that creates a program, compiles shaders and 
binds them to that program as follows:
```C++
/*
IN: vertexShader         Source code for the vertex shader
IN: fragmentShader       Source code for the fragment shader
*/
static unsigned int createShader(const std::string& vertexShader,
                                 const std::string& fragmentShader)
{
   unsigned int program = glCreateProgram();
   unsigned int vs = CompileShader(vertexShader, GL_VERTEX_SHADER);
   unsigned int fs = CompileShader(fragmentShader, GL_FRAGMENT_SHADER);

   glAttachShader(program, vs);
   glAttachShader(program, fs);
   glLinkProgram(program);
   glValidateProgram(program);

   glDeleteShader(vs);
   glDeleteShader(fs);

   return program;
}
```

Of course, these functions need to be provided with actual shader source code in the 
form of a string. In this example, we have explicitly written the strings in C++. Given this
the state preparation part of the program now looks like this:

```C++
   /* Initialize & populate Vertex buffer */

   float positions[6] = {
      -0.5f, -0.5f,
       0.0f,  0.5f,
       0.5f, -0.5f
   };

   unsigned int buffer;
   glGenBuffers(1, &buffer);
   glBindBuffer(GL_ARRAY_BUFFER, buffer);
   glBufferData(GL_ARRAY_BUFFER, sizeof(positions), positions, GL_STATIC_DRAW);

   glEnableVertexAttribArray(0);
   glVertexAttribPointer(0, 2, GL_FLOAT, GL_FALSE, sizeof(float)*2, 0);

   std::string vertexShader =
      "#version 330 core\n"
      "\n"
      "layout(location = 0) in vec4 position;\n"
      "\n"
      "void main()\n"
      "{\n"
      "  gl_Position = position;\n"
      "}\n";

   std::string fragmentShader =
      "#version 330 core\n"
      "\n"
      "layout(location = 0) out vec4 color;\n"
      "\n"
      "void main()\n"
      "{\n"
      "  color = vec4(1.0, 0.0, 1.0, 1.0);\n"
      "}\n";

   unsigned int shader = createShader(vertexShader, fragmentShader);
   glUseProgram(shader);
```
In the future, we clearly need a smoother way to include shaders, since this method
has no syntax highlighting and manually inputting newlines is a hassle. 

At this point, the usual draw loop will produce a colored triangle, depending on what values
we gave the fragment shader.
```C++
/* While loop until window is closed */
while (!glfwWindowShouldClose(window)) {
    /* Render here */
    glClear(GL_COLOR_BUFFER_BIT);

    glDrawArrays(GL_TRIANGLES, 0, 3);

    /* Swap front and back buffers */
    glfwSwapBuffers(window);

    /* Poll for and process events */
    glfwPollEvents();
   }
```

![](./pictures/purpleTriangle.PNG)

### Improving shader workflow

To improve the shader workflow I installed the VS extension https://marketplace.visualstudio.com/items?itemName=DanielScherzer.GLSL
and I store my shaders in .vert or .frag files. This way I get some syntax highlighting.

I parse the files using a function that was supposed to be simple:

```C++
static std::string makeSource(std::string filepath)
{
   std::ifstream fileStream(filepath);
   std::string fileContent(std::istreambuf_iterator<char>{fileStream},
                           std::istreambuf_iterator<char>{});
   return fileContent;
}
```
Note that I am using strange brackets for my arguments, which is necessary due to something called
the "most vexing parse" (see https://en.wikipedia.org/wiki/Most_vexing_parse). Huh. Today
I learned something.

Anyway, now shaders are called in the following fashion
���C++
  std::string vertexShader = makeSource("./shaders/basic.vert");
   std::string fragmentShader = makeSource("./shaders/basic.frag");
���
which while it works in VS, will not work for the .exe. I guess I should set up some kind of 
"copy shaders on compile" directive in the VS build system.


### Index buffers in OpenGL

An index buffer is a way to reuse vertices. For example, consider the case of
wanting to draw a square. This is done by drawing two triangles (the obsession with triangles
is because when embedded in 3d, a triangle is the only 2d geometric shape that uniquely defines
a flat plane, and thus a surface normal and other useful stuff). When drawing two triangles
we will want to declare six vertices (right?), such as 
```C++
   float positions[12] = {
      -0.5f, -0.5f, // Triangle 1
       0.5f, -0.5f,
       0.5f,  0.5f,
       0.5f,  0.5f, // Traingle 2
      -0.5f,  0.5f
      -0.5f, -0.5f,
   };
```
We see that there are two vertices being reused, and it's costing us four extra floats (or 
50% MORE!) of memory. For a more complecated polygon (or 3d object), this will only get worse - any closed
surface tesselated by trangles will have each vertex duplicated a least once, more than doubling
memory consumption. To get around this, we can use so-called index buffer - realizing that
it takes less extra data to store a separate directory fo the indices we want from a set of vertices
than it takes to reuse the indices a bunch of extra times. This is especially true if we have fewer
vertices than can fit in a 2 `sizeof(float)` integer (fewer than 18,446,744,073,709,551,615). 
I am not sure I have enough VRAM to store that many floats, so an index buffer is obviously good.

To use an index buffer in openGL, we create a second buffer of type `GL_ELEMENT_ARRAY_BUFFER`
and then use `glDrawElements(GL_TRIANGLES, sizeof(cornerIndices), GL_UNSIGNED_INT, NULL);` for
the draw call. 

![](./pictures/squareCode.PNG)

And of course, the result is the following

![](./pictures/square.PNG)

Index buffers are supposedly what is used in real projects too, which is probably obvious considering
the very significant memory savings. 


### How do I actually debug openGL?

If you make a mistake with openGL, you tend to just get a black screen. OpenGL is actually perfectly OK 
at detecting errors, but it is less OK at sending them away from the GPU unpromted. OpenGL provides two 
utilities for this purpose:

 * glGetError
   * Polls GL for one error (arbitrarily picked if there is more than one error)
   * Returns the error in the form of an error flag (a number corresponding to verbose #defines)
   * Gotta call it until you get the GL_NO_ERROR flag to ensure you don't miss an error._
 * glDebugMessageCallback
   * Amazing
   * Literally explains to you in english what you did wrong.
   * Only compatible with openGL 4.3 and later (pretty late). My GPU supports this =)

I don't care about backward compatibility, so I will go with the latter. 

An example implementation of this can be found on the openGL wiki:

```C++
/*
* GL callback function to print debugging infromation. Taken from
* https://www.khronos.org/opengl/wiki/Example/OpenGL_Error_Testing_with_Message_Callbacks
*/
void GLAPIENTRY
MessageCallback(GLenum source,
   GLenum type,
   GLuint id,
   GLenum severity,
   GLsizei length,
   const GLchar* message,
   const void* userParam)
{
   fprintf(stderr, "GL CALLBACK: %s type = 0x%x, severity = 0x%x, message = %s\n",
      (type == GL_DEBUG_TYPE_ERROR ? "** GL ERROR **" : ""),
      type, severity, message);
}
```

And then, during initialization of our glfw context, we add
```C++
   /* Enable debug output */
   glEnable(GL_DEBUG_OUTPUT);
   glDebugMessageCallback(MessageCallback, 0);
```
Note that this has to be done after the window's context has been made current.

### Uniforms

Right now we are hardcoding the color of our square. It would be nice if
we could pass the color from the CPU context to the GPU. This could be done via vertex buffers ("attricbutes"). 
The problem is that attributes are meant to be done once per vertex.

Uniforms are instead set once per draw, letting us edit a few "global" (as seen from vertices)
 variables once per draw. The shader is modified to use the uniform variable with an 
qualifier "uniform". 

```C
#version 430 core

layout(location = 0) out vec4 color;

uniform vec4 uColor;

void main()
{
color = uColor;
}
```

To write to this variable in our program, we get a signed integer that indicates it's memory adress on
the GPU side by using
```C++
 // uColor is defined in basic.frag
   GLint colorLocation = glGetUniformLocation(shader, "uColor");
   assert(colorLocation != -1);
   glUniform4f(colorLocation, 0.0, 0.0, 0.0, 1.0);
```
Note that the variable name that is fiven to `glGetUniformLocation` must be exactly the same as the `uniform vec4 uColor;`
in our shader. 

After this, a slight modification of the draw loop

```C++
   /* While loop until window is closed */
   float colorTimeStep = 1 / 30.0;
   float time = 0;
   while (!glfwWindowShouldClose(window)) {
      /* Render here */
      glClear(GL_COLOR_BUFFER_BIT);

      glUniform4f(colorLocation, 1.0*sinf(time), 1.0 * sinf(2 * time), 1.0 * sinf(3 * time), 1.0);
      glDrawElements(GL_TRIANGLES, sizeof(cornerIndices), GL_UNSIGNED_INT, NULL);

      /* Swap front and back buffers */
      glfwSwapBuffers(window);

      /* Poll for and process events */
      glfwPollEvents();
      time += colorTimeStep;
   }
   glfwTerminate();
   return 0;
```

The result? Rave square. 

![](./pictures/raveSquare.gif)

I also used �glfwSwapInterval(1);� to sync the refreshrate of my program to my screen. In a real application
you would not tie the animation speed to frames as in this example and instead use real time (at least according
to some clock).

### Vertex arrays


Vertex arrays are similar to vertex buffers. Vertex arrays are special for OpenGL. Basically, this lets
us get away with fewer uses of `glVertexAttribPointer`. which needs to be re-called every time we rebind
our buffers. Vertex arrays are basically way to bind the vertex buffer and attribute array together right away.

Vertex array objects are actually mandatory, but our openGL is running with the "Compatibility profile",
so it automatically creates the Vertex array object on startup. 
We "specified" this by creating our openGL context in the default mode. 
```C++
   window = glfwCreateWindow(1280, 960, "Hello World", NULL, NULL);
```
Details here https://www.khronos.org/opengl/wiki/Vertex_Specification

Nvidia does not let me use anything except compatibility mode for openGL, so I can
not prove the above statement. To create & bind the vertex array object manually, we just do

```C++
   unsigned int vertexArrayObject;
   glGenVertexArrays(1, &vertexArrayObject);
   glBindVertexArray(vertexArrayObject);
```
The vertex array object (VAO) is linked to your vertex buffer by the function by the function call
```C++
   glVertexAttribPointer(0, 2, GL_FLOAT, GL_FALSE, sizeof(float)*2, 0);
```
We can now rebind the same vertex buffer without calling `glBindBuffer(GL_ARRAY_BUFFER, 0);�.
The vertex array object also stores the vertexAttribPointer data, so being able to rebind the entire
VAO in one call is an efficiency gain.
```C++
   /* reset state for demo purposes */
   glBindVertexArray(0);
   glUseProgram(0);
   glBindBuffer(GL_ARRAY_BUFFER, 0);
   glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);

   /* While loop until window is closed */
   float colorTimeStep = 1 / 30.0;
   float time = 0;
   while (!glfwWindowShouldClose(window)) {
      /* Render here */
      glClear(GL_COLOR_BUFFER_BIT);

      /* remake state for demo */
      glUseProgram(shader);
      glBindVertexArray(vertexArrayObject);
      glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, indexBuffer);
      /* Code works without binding the vertex buffer or redoing vertexAttribPointer! */
      glUniform4f(colorLocation, 1.0*sinf(time), 1.0 * sinf(2 * time), 1.0 * sinf(3 * time), 1.0);
      glDrawElements(GL_TRIANGLES, sizeof(cornerIndices), GL_UNSIGNED_INT, NULL);

      /* Swap front and back buffers */
      glfwSwapBuffers(window);

      /* Poll for and process events */
      glfwPollEvents();
      time += colorTimeStep;
   }
```

### Abstraction into classes

At this point, we can draw a square, but drawing more complicated things is looking to be
really impractical. To simplify the process of drawing more complicated
things, it is time fore some abstraction. 

The goal is that we make a c++ class out of the vertex buffer, 
the vertex array and the index buffer. We will also make a class
to store the layout of the data ("first, two floats in the location
labelled 0, then something else at the location 1" and so on for 
each vertex). In addition, abstraction will make it easier to
understand the code, and relate it to the "basic theory" of the
pervious sections. 

After abstraction, all of the setup plus the draw
call could be condensed into the below form.

```C++
      vertexArray va;
      vertexBuffer vertices(positions, sizeof(positions));
      vertices.bind();

      vertexBufferLayout layout;
      layout.push<float>(2, GL_FALSE);
      va.addBuffer(vertices, layout);

      indexBuffer ib(cornerIndices,
         sizeof(cornerIndices) / sizeof(cornerIndices[0]));
      ib.bind();


      shader s("./shaders/basic.vert", "./shaders/basic.frag");
      s.bind();

      vertices.unbind();
      s.unbind();
      va.unbind();
      ib.unbind();

      renderer mainRenderer;

      /* While loop until window is closed */
      float colorTimeStep = 1 / 30.0f;
      float time = 0;
      while (!glfwWindowShouldClose(window)) {
         mainRenderer.clear();

         s.bind();
         s.setUniform4f("uColor", 1.0f * sinf(time), 0.5f, 0.5f, 1);

         mainRenderer.draw(va, ib, s);
         /* Swap front and back buffers */
         glfwSwapBuffers(window);

         /* Poll for and process events */
         glfwPollEvents();
         time += colorTimeStep;

         va.unbind();
      }
   }
```

Let us go over all of the classes one by one,
and then return one extra time to this snippet 
of code. The classes on a slogan level are as follows:

* Vertex buffer
  * The vertex buffer class takes a pointer to a buffer of
    of data and the size of that data. It then creates,
    maintains and destroys that data in VRAM via the openGL interface.
    A call to glDeleteBuffer is part of the destructor of the
    vertex buffer. This actually holds for the other buffers
    as well, so I will not repeat this information.

* Index buffer
  * The index buffer class does exactly what the vertex 
    does, except for the index buffer that is used to
    draw data. 

* Vertex buffer layout
  * The vertex buffer layout lets us push new vertex 
    features one by one, automatically adjusting the stride
    and index information required by glVertexAttribPointer.

* Vertex array
  * The vertex array class takes a vertex buffer and
    a vertex buffer layout and deduces the correct
    calls to glEnableVertexAttribArray and glVertexAttribPointer
    to inform the GPU of the intended memory layout.
    
* Shader
  * The shader class takes paths to fragment and
    vertex shaders and compiles the assoicated program.
    It also takes care of cleanup in it's destructor and
    wraps the glUniform functions.  

* Renderer
  * The renderer, when handed a vertex array, an index buffer
    and a shader, draws whatever has been specified.

Let us now give a more detailed account of the classes, including code.


#### Vertex buffer

In general, I think the vertex buffer abstraction
is very simple. Basiaclly, we are just wrapping a few
openGL function calls, as well as maintaing the 
identification int that openGL hands us as a class
member variable "rendererID" (as in, ID handed to us
by the graphics rendering API). 

```C++
#pragma once

class vertexBuffer
{
private:
   unsigned int rendererId;

public:
   vertexBuffer(const void* data, unsigned int size);
   ~vertexBuffer();

   void bind() const;
   void unbind() const;
};
```
The constructor just needs the usual three lines of
code that we use to set up a vertex buffer. 
```C++
vertexBuffer::vertexBuffer(const void* data, unsigned int size)
{
   glGenBuffers(1, &rendererId);
   glBindBuffer(GL_ARRAY_BUFFER, rendererId);
   glBufferData(GL_ARRAY_BUFFER, size, data, GL_STATIC_DRAW);
}
```
The destructor, however, calls a new function for us:
```C++
vertexBuffer::~vertexBuffer()
{
    glDeleteBuffers(1, &rendererId);
}
```
The reason we did not have to call this function before
is because we have not written a program that doesn't
terminate by the time we are done with the buffer.
This way we get a nice "smart pointer"-like cleanup of
VRAM whenever a vertex buffer goes out of scope. 

The bind and unbind functions are literally just openGL
function call wrappers. 
```C++
void vertexBuffer::bind() const
{
   glBindBuffer(GL_ARRAY_BUFFER, rendererId);
}

void vertexBuffer::unbind() const
{
   glBindBuffer(GL_ARRAY_BUFFER, 0);
}
```


#### Index buffer
The index buffer is abstracted almost identically
to the vertex buffer, except we also store a number
sayong how many indices we have stored. This value
has an associated getter function so that users can
access this information about the buffer. 

```C++
#pragma once

class indexBuffer
{
private:
   unsigned int rendererId;
   unsigned int indexCount;
public:
   indexBuffer(const unsigned int* data, unsigned int count);
   ~indexBuffer();

   void bind() const;
   void unbind() const;

   inline unsigned int getCount() const { return indexCount; };
};
```
The implementation is basically identical to the vertexbuffer
```C++
indexBuffer::indexBuffer(const unsigned int* data, unsigned int count)
   : indexCount(count)
{

   glGenBuffers(1, &rendererId);
   glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, rendererId);
   glBufferData(GL_ELEMENT_ARRAY_BUFFER, count * sizeof(unsigned int),
                data, GL_STATIC_DRAW);
}

indexBuffer::~indexBuffer()
{
   glDeleteBuffers(1, &rendererId);
}

void indexBuffer::bind() const
{
   glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, rendererId);
}

void indexBuffer::unbind() const
{
   glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);
}
```


#### Vertex buffer layout
The vertex buffer layout is a bit more involved, and
we even use a bit of template magic to make it "nice".
The fundamental unit of the layout is **one vertex feature**.
I encode this in the following struct
```C++
struct vertexBufferElement
{
   unsigned int count;
   unsigned int type;
   bool normalized;

   static unsigned int getSizeOfType(unsigned int type)
   {
      switch (type)
      {
      case GL_FLOAT:         return sizeof(GLfloat);
      case GL_UNSIGNED_INT:  return sizeof(GLuint);
      case GL_UNSIGNED_BYTE: return sizeof(GLubyte);
      }
      assert(false);
      return 0;
   }
};
```
The idea is this: 
A vertex feauture is describe by *count* values, of some datatype
*type*, and the features may or may not come prenormalized. The method
getSizeOfType
is necessary for the vertex array to correctly calculate layout features
such as stride and offset, we will see it's use shortly. The vertex buffer
layout class looks as follows:
```C++
class vertexBufferLayout
{
private:
   std::vector<vertexBufferElement> elements;
   unsigned int stride;
public:
   vertexBufferLayout()
      : stride(0) {}

   template<typename T>
   void push(unsigned int count, bool normalized)
   { // Don't compile if unsupported type is provided
      static_assert(false);
   }

   template<>
   void push<float>(unsigned int count, bool normalized)
   {
      elements.push_back({ count, GL_FLOAT, normalized });
      stride += vertexBufferElement::getSizeOfType(GL_FLOAT) * count;
   }

   template<>
   void push<int>(unsigned int count, bool normalized)
   {
      elements.push_back({ count, GL_UNSIGNED_INT, normalized });
      stride += vertexBufferElement::getSizeOfType(GL_UNSIGNED_INT) * count;
   }


   template<>
   void push<unsigned char>(unsigned int count, bool normalized)
   {
      elements.push_back({ count, GL_UNSIGNED_BYTE, normalized });
      stride += vertexBufferElement::getSizeOfType(GL_UNSIGNED_BYTE) * count;
   }

   inline std::vector<vertexBufferElement> getElements() const
   {
      return elements;
   }

   inline unsigned int getStride() const& { return stride; }
};
```
The class maintains a vector of vertex buffer elements, and keeps track of
the *stride*. The stride is the size of the information that pertains to 
one vertex. The stride has an associated getter. 
In addition to this, the layout has methods for "pushing" vertex properties
of various datatypes. The act of pushing here adds one element to the class-maintened
vector of vertexBufferElements. The methods also increase the stride
by the *count* times the size of the datatype pushed, i.e. by the size of the vertex.


#### Vertex array

The vertex array, like previous openGL object that have been implemented as
C++ classes, has constructors, destructor, bind and undbind functions
that call the relevant open GL functions. In additon it has one special 
function named `addBuffer` whose responsibility is to bind one vertex
buffer to the vertex array together with the layout information. 
```C++
void vertexArray::addBuffer(const vertexBuffer& vb,
                            const vertexBufferLayout& layout)
{
   vb.bind();
   const auto& elements = layout.getElements();
   unsigned int offset = 0;
   for (unsigned int i = 0; i < elements.size(); i++)
   {
      const auto& element = elements[i];
      glEnableVertexAttribArray(i);
      glVertexAttribPointer(i, element.count, element.type,
                            element.normalized, layout.getStride(),
                            (const void*)offset);
      offset += vertexBufferElement::getSizeOfType(element.type)
                  * element.count;
   }
}
```
In summary, for each vertex array feature ("element") in the layout,
we make the relevant calls to `glEnableVertexAttribArray` and `glVertexAttribPointer`
with variably types, normalization, count stride and offsets fetched
or computed from the layout. 

#### Shader

The Shader class takes the paths to two shader files to compile and bind a shader. We can then
bind, unbind the shader and access uniforms through the API provided by the class. For an
overview of the shader, let us check the header
```C++
#pragma once
#include <string>
#include <unordered_map>
#include <glm/glm.hpp>

class shader {
private:
   unsigned int rendererID;
   std::string vertexPath;
   std::string fragmentPath;
   unsigned int getUniformLocation(const std::string name);
   unsigned int compileShader(const std::string& source, unsigned int type);
   std::string makeSource(std::string filepath);
   unsigned int createShader(const std::string& vertexShader,
                             const std::string& fragmentShader);
   std::unordered_map<std::string, unsigned int> uniformLocationCache;
public:
   shader(const std::string& vertexShaderPath,
          const std::string& fragmentShaderPath);
   ~shader();

   void bind() const;
   void unbind() const;

   void setUniform1i(const std::string name, int v0);
   void setUniform1f(const std::string name, float v0);
   void setUniform4f(const std::string name,
                     float v0, float v1, float v3, float v4);
};
```
Aside from the obvious, `getUniformLocation` actually 
maintains a hash-map to avoid having to re-call glGetUniform
for uniforms that we already know the location of, because the openGL
call here costs many, many times more cycles than a hash table lookup. 

Everything else is either all functions, or slight variations thereof. 
For illsutrative purposes, here is an example implementation
of one of the uniform setters:
```C++
void shader::setUniform4f(const std::string name, float r, float g, float b, float alpha)
{
   unsigned int location = getUniformLocation(name);
   glUniform4f(location, r, g, b, alpha);
}

unsigned int shader::getUniformLocation(const std::string name)
{
   if (uniformLocationCache.find(name) != uniformLocationCache.end())
   {
      // Keep a cache of already known locations because
      // glGetUniformLocation is so slow we want to avoid calling it a lot.
      return uniformLocationCache[name];
   }
   unsigned int location = glGetUniformLocation(rendererID, name.c_str());
   uniformLocationCache[name] = location;
   if (location == -1) {
      std::cout << "Warning, uniform " << name <<
         " doesn't exist!" << std::endl;
   }
   return location;
}
```


#### Renderer

The renderer takes a vertex array, an index buffer and a shader
and then draws whatever they describe. The entire class is only a
few lines:
```C++
void renderer::clear() const
{
   glClear(GL_COLOR_BUFFER_BIT);
}

void renderer::draw(const vertexArray& va, const indexBuffer& ib,
                     const shader& shader) const
{
   shader.bind();
   va.bind();
   ib.bind();
   glDrawElements(GL_TRIANGLES, ib.getCount() * sizeof(GLuint),
                  GL_UNSIGNED_INT, NULL);
}
``` 
but can save us quite a few lines of code when
we have multiple vertex arrays that we want to draw. 

#### Blending in OpenGL

Blending specifies how colors from the buffer currently being drawn are to
be mixed by what is already in the buffer. This is relevant when we are drawing something
with alpha (opacity) not equal to one. The most standard method is to multiply the (r, g, b) part
of the source color vector by alpha, and the already existing color (r, g, b) by (1 - alpha). 
This is specified to openGL by the following:
```C++
      glEnable(GL_BLEND);
      glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
```
Of course, this kind of blending might be too basic if you want to emulate an imperfect glass surface,
or just glass at an angle with a refractive index different to that of air. 

#### Projection in openGL

In openGL, a projection is a map from the space of coordinates in which you specify your 
vertices and their positions to a two dimensional square that covers [-1, 1] in the x and y directions.
This is a projection since we go down one dimension. The most basic projection is the orthographic 
projection, in which things far away retain their size - i.e. the line of projection are orthogonal
to the camera plane. Using the maths library GLM it is quite simple to define an orthographic 
projection: 
```C++
      glm::mat4 proj = glm::ortho(-2.0f, 2.0f, -1.5f, 1.5f, -1.0f, 1.0f);
        ...    
      s.bind();
        ...
      s.setUniformMat4f("uModelViewProjectionMatrix", proj);
```
This matrix simply scales the coordinates in the vertex buffer so that the interval x:[-2, 2] is mapped
to [-1, 1] and the interval y:[-1.5, 1.5] is mapped to the interval [-1, 1]. I have augmented
the shade class with a wrapper for the openGL set mat4 uniform function. The main reason to
use GLM for the matrix class is that for some reason openGL has column-major-ordered matrices,
which is the transpose of how I would intuitively implement them if I did it myself. To communicate this 
information to the GPU, we use a uniform to pass the matrix, and multpily the positions vertexbuffer
by it. 
```C++
#version 430 core

layout(location = 0) in vec4 position;
layout(location = 1) in vec2 texCoord;

out vec2 varyingTextureCoord;
uniform mat4 uModelViewProjectionMatrix;

void main()
{
    gl_Position = uModelViewProjectionMatrix * position;
    varyingTextureCoord = texCoord;
};
```
For this particular projection, the main gain is that we can work on the CPU side in coordinates
such that a an object that is 1-by-1 in our coordinates actually has the same height and width. 


### Model-View-Projection in openGL

Model-View-Projection is a standard pipeline of
transformations from the "scene space" where we 
imagine our objects as we write code and the "camera space"
where those objects are translated to the screen. 

We already covered the projection, but what is the 
model and the view?



#### View

The view matrix is usually split into translation, rotation
and scale. It simulates moving the "camera" around. 

If you're like me, you'll go "wait, translation isn't linear! What does
the matrix for that look like?". I recommend consulting the
top of the relevant wikipedia page for that, the solution
is very simple https://en.wikipedia.org/wiki/Translation_(geometry) . 


#### Model

The model matrix is usually split into translation, rotation
and scale. It simulates the moving around of the object
of interest. In principle, since all transformation are in reality
being applied to the model of our graphics, this is the same as
the View matrix, but it is conceptually different (distinction between
"passive" and "active" versions of the same transformation)
It may in some cases be more convenient to think in one way or the other.

#### ImGui + OpenGL

ImGui is a library for debugging and modifying your
graphics code in runtime with a graphical UI. Basically,
you download the library, include the parts relating to your 
graphics framework of choice (openGL, Vulken, openCL, etc). 
To use it you include some headers
```C+++
#include <imgui/imgui.h>
#include <imgui/imgui_impl_glfw.h>
#include <imgui/imgui_impl_opengl3.h>
```
Then, the first step is to initialize an ImGui context
```C++
  ImGui::CreateContext();
  ImGui_ImplGlfw_InitForOpenGL(window, true);
  ImGui_ImplOpenGL3_Init("#version 430");
```
Then, early in your draw loop (but after mainRenderer.clear() )
you make a new imgui frame:
```C++
    ImGui_ImplOpenGL3_NewFrame();
    ImGui_ImplGlfw_NewFrame();
    ImGui::NewFrame();
```
Now, to give imgui the ability to modify your graphics, you need to
give it pointers to variables that control your uniforms. See the following code snippet:
```C++
  ImVec4 clear_color = ImVec4(0.45f, 0.55f, 0.60f, 1.00f);
  glm::vec3 imGuiViewParam(0.0f);
  glm::vec3 imGuiViewParam2(0.0f);
  while (!glfwWindowShouldClose(window)) {
        ...
    glClearColor(clear_color.x, clear_color.y, clear_color.z, clear_color.w);


    s.bind();
    s.setUniform1i("uTexture", 0);
    view = glm::translate(glm::mat4(1.0f), imGuiViewParam);
    MVP = proj * view * model;
    s.setUniformMat4f("uModelViewProjectionMatrix", MVP);
    mainRenderer.draw(va, ib, s);


    MVP = proj * view * model;
    s.setUniformMat4f("uModelViewProjectionMatrix", MVP);
    mainRenderer.draw(va, ib, s);
    /* Poll for and process events */
    glfwPollEvents();
    time += colorTimeStep;
    va.unbind();

    {
    static float f = 0.0f;
    static int counter = 0;

    ImGui::Begin("Hello, world!");                          // Create a window called "Hello, world!" and append into it.

    ImGui::Text("This is some useful text.");               // Display some text (you can use a format strings too)
    ImGui::Checkbox("Demo Window", &show_demo_window);      // Edit bools storing our window open/close state
    ImGui::Checkbox("Another Window", &show_another_window);

    ImGui::SliderFloat3("Translation", &imGuiViewParam.x, -1.0f, 1.0f);  // Edit 1 float using a slider from 0.0f to 1.0f
    ImGui::SliderFloat3("Translation", &imGuiViewParam.x, -2.0f, 1.0f);  // Edit 1 float using a slider from 0.0f to 1.0f
    ImGui::ColorEdit3("clear color", (float*)&clear_color); // Edit 3 floats representing a color

    if (ImGui::Button("Button"))                            // Buttons return true when clicked (most widgets return true when edited/activated)
        counter++;
    ImGui::SameLine();
    ImGui::Text("counter = %d", counter);

    ImGui::Text("Application average %.3f ms/frame (%.1f FPS)", 1000.0f / ImGui::GetIO().Framerate, ImGui::GetIO().Framerate);
    ImGui::End();
    }

    ImGui::Render();
    ImGui_ImplOpenGL3_RenderDrawData(ImGui::GetDrawData());

    /* Swap front and back buffers */
    glfwSwapBuffers(window);
  }
```
We see that by handing pointers to the SliderFloat3 and ColorEdit3 functions, we are
are able to modify these uniforms in real time using the imgui interface:

![](./pictures/sliderDemo.gif)

#### Experiments framework

By using ImGui we can set up a menu where we can use a simple
drop down menu to swap between rendering different working examples. 
This way we do not have to modify the main file everytim
we do a new experiment. 

Design wise, we can have the "scenes" be classes that implement a unified
interface that can be used by the main file. We define the following
pure virtual class to define our desired interface:

```C++
#pragma once

namespace test {
   class test
   {
   public:
      test() {};
      virtual ~test() {};

      virtual void onUpdate(float deltaTime) {}
      virtual void onRender() {}
      virtual void onImguiRender() {}
   };
}
```

Where the three functions are responsible for:
 * onUpdate:
   * updating the state of the Vertex and Index Buffers
   * updating uniforms
 * onRender
   * Rendering everything defined by the vertex & index buffers
 * onImguiRender
   * Handling all of the Imgui render stuff, such as sliders,
     FPS counters and other gadgets.

This let us easily create test scenes for very simple functionality,
such as just verifying glClearColor:
```C++
test::testClearColor::testClearColor()
   : mClearColor{ 0.5f, 0.5f, 0.5f, 1.0f }
{

}

test::testClearColor::~testClearColor()
{
}

void test::testClearColor::onUpdate(float deltaTime)
{
}

void test::testClearColor::onRender()
{
   glClearColor(mClearColor[0], mClearColor[1],
                mClearColor[2], mClearColor[3]);
   glClear(GL_COLOR_BUFFER_BIT);
}

void test::testClearColor::onImguiRender()
{
   ImGui::Begin("Pick color");

   ImGui::ColorEdit4("clear color", &mClearColor[0]);
   ImGui::Text("Application average %.3f ms/frame (%.1f FPS)",
      1000.0f / ImGui::GetIO().Framerate, ImGui::GetIO().Framerate);

   ImGui::End();

   ImGui::Render();
   ImGui_ImplOpenGL3_RenderDrawData(ImGui::GetDrawData());
}
```
As well as more complicated scenarios, such as this spinning polygon:
![](./pictures/spinningPolygonDemo.gif) .


Of course, just rendering scenarios is not enough, we also want some
kind of wrapper that lets us browse test scenarios and move between 
them dynamically. This can be implemented as follows: we
create an `std::vector` that holds the necessary information for
a testcase and it's menu information in each element. This is used to
construct a main menu with a set of buttons for each of the test cases. When
clicked, the buttons start up the testcase. 

The vector should be able to take a mix of different testcases,
so we make use of the virtual class `test::test`, and we'll have to 
allocate the testcase classes on the heap to make use of polymorphism.
To make this process a bit cleaner, (and avoid allocating memory for all
test class pointers), we will wrap this functionality in a class `test::testMenu`

```C++
class testMenu : public test
   {
   public:
      testMenu(test*& testMenuPointer);

      void onRender() override;
      void onImguiRender() override;

      template <typename T>
      void registerTest(std::string name)
      {
         std::cout << "Creating testcase: " << name << std::endl;
         mTests.push_back(std::make_pair(name, []() { return new T(); }));
      }

   private:
      test*& mCurrentTest;
      std::vector<std::pair<std::string, std::function<test * ()>>> mTests;
      renderer mRenderer;
   };
```
Note that we've made the second member of the `std::pair` not a pointer to a testcase,
but a lambda that allocates a new testcase on the heap. In a draw-loop we then we use
```C++
if (currentTest != menu && ImGui::Button("<--- return"))
{
    delete currentTest;
    currentTest = menu;
}
```
to delete this heap-allocated pointer and refer the currentTest pointer
back to the main menu class itself. To fulfill the loop, all we have to
do when we press one of the test case names in the menu, is set 
currentPointer to point to a testcase. This is achieved by the following
```C++
void test::testMenu::onImguiRender()
{
   for (auto& test : mTests)
   {
      if (ImGui::Button(test.first.c_str()))
      {
         mCurrentTest = test.second();
      }
   }
}
```
where mCurrentTest is a member of `testMenu` of the type
`test*&`, i.e. a reference to the 'currentTest' in the outer scope.
For the future, I might consider having the "testMenu" object
maintain both pointers, for the simple reason that I think
that it is bad encapsulation for a class to rely on modifying
a reference to an outside variable. Understanding the logic here requires
one to look both at the usecase and the class.

Below is a demo of this test menu in action, with two testcases
added to the list. 
![](./pictures/choiceDemo.gif)

Another extension that can be made to the test framework is letting
each test scene modify the resoltuion of the gflw context. For
this, we add some pure virtual function to the test interface. The
reason for making them pure virtual is that we want to force  each test
to take responsibility for setting it's own resolution (and clearing
any changes it made to the global state of the glfw context).
```C++
   class test
   {
   public:
      test() {};
      virtual ~test() {};
      // Set down any global changes made to the openGL context window
      virtual void onSetup(GLFWwindow* window) = 0;
      // Update state of vertex/index buffers.
      virtual void onUpdate(float deltaTime) {}
      // Feed buffers to renderers
      virtual void onRender() {}
      // Imgui logic + rendering sohuld go here.
      virtual void onImguiRender() {}
      // Tear down any global changes made to the openGL context window
      virtual void onTearDown() = 0;
   };
```
An example implementation of `onSetup` is as follows:
```C++
void test::testSpinningXagon::onSetup(GLFWwindow* window)
{
   glfwSetWindowSize(window, 1440, 1080);
   glViewport(0, 0, 1440, 1080);
}
```

### Batch rendering
In situations where we have many small objects to draw, the overhead
of a draw call to the GPU and many uploads of small amounts of data
can represent a very significant overhead. In this case it might be good
to do "batching", in which we put several objects that we wish to draw into a
single vertex-index buffer pair. This is in principle simple: if
we want to draw two triangles the index buffers would looks as follows:
```C++
float positions[3*2*2] = {ax, ay, bx, by, cx, cy
                          dx, dy, ex, ey, fx, fy};
unsigned int indices[3*2] = {0, 1, 2,
                             3, 4, 5}
```
Of course, a more general way of batching multiple objects 
is desirable. For example it would be nice to define each object one at a time
and then just push them to the batch renderer one at a time. Depending on the flexibility
of openGL one might need to instantiate one batch renderer per 
bufferLayout.

If we want the two triangles to be drawn in different colors, 
we will have to somehow pass two colors to the shader so that it
knows what to draw. One way of doing this is by adding a color feature
to the vertex buffer, so that for each vertex, we give a color.
That is, the vertex buffer now looks something like
```C++
                   //position (2f) //Color (rgba)
   mVertexBuffer{ -1.05f, -0.4f, 0.1f, 0.3f, 1.0f, 1.0f,
                  -0.25f, -0.4f, 0.1f, 0.3f, 1.0f, 1.0f,
                  -0.25f,  0.4f, 0.1f, 0.3f, 1.0f, 1.0f,
                  -1.05f,  0.4f, 0.1f, 0.3f, 1.0f, 1.0f,

                   0.25f, -0.4f, 1.0f, 0.94f, 0.24f, 1.0f,
                   1.05f, -0.4f, 1.0f, 0.94f, 0.24f, 1.0f,
                   1.05f,  0.4f, 1.0f, 0.94f, 0.24f, 1.0f,
                   0.25f,  0.4f, 1.0f, 0.94f, 0.24f, 1.0f },
```
Then, we rewrite the vertex shader to pass a varying color to
the fragment shader, which recieves it
```C++
#version 430 core

layout(location = 0) in vec4 position;
layout(location = 1) in vec4 color;

uniform mat4 uModelViewProjectionMatrix;
uniform float uAngle = 0;

out vec4 v_color;

void main()
{
	v_color = color;
	gl_Position = uModelViewProjectionMatrix * position;
};
```
```C++
#version 430 core

layout(location = 0) out vec4 color;

in vec4 v_color;

void main()
{
	color = v_color;
}
```
While we're at it, we can take the opportunity to  look into how the fragment
shader interpolates between the vertices with the help of imgui. If we set
different colors at the vertices (in the below gif, with the help of sliders),
we see that there is clearly some linear interpolation going on.

![](./pictures/colorBatchingAndVertexDemo.gif)

It is obvious that we could add some fun time dependence here for some
fun visuals. 

Additionally, we can batch textures. This is done using the (up to 32 in
the openGL specificaiton) texture slots of the GPU. In more advanced applications
one might also use a "texture atlas" in which many textures are bundled into one
object that is then uplaoded to the GPU. In my example, I went with 
one texture per slot sinc that is much easier. 


### Dynamic buffers. 



### Layer system

It would be good to implement reusable modules that handle
for example camera movement in response to keyboard events.
Most simple would be if I in one line could add a layer
to a testcase, and this would trigger the relevant onUpdate,
onRender and onImguiRender calls automatically. 


Things to learn/look up
* Frame buffers
* Batch rendering
* faster uniforms
* lighting
* shadow mapping
* post processing