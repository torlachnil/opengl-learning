#version 430 core

layout(location = 0) in vec4 position;
layout(location = 1) in vec2 texCoord;

out vec2 varyingTextureCoord;
uniform mat4 uModelViewProjectionMatrix;

void main()
{
	gl_Position = uModelViewProjectionMatrix * position;
	varyingTextureCoord = texCoord;
};