#version 430 core

layout(location = 0) out vec4 color;

in vec4 v_color;
in vec2 v_texCoord;
in float v_texIndex;
uniform sampler2D uTextures[2];

void main()
{
	int index = int(v_texIndex);//Is this safe? float might be 0.99999
	color = texture(uTextures[index], v_texCoord);
}