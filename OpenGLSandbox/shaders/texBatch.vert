#version 430 core

layout(location = 0) in vec4 position;
layout(location = 1) in vec4 color;
layout(location = 2) in vec2 texCoord;
layout(location = 3) in float texIndex;

uniform mat4 uModelViewProjectionMatrix;
uniform float uAngle = 0;

out vec4 v_color;
out vec2 v_texCoord;
out float v_texIndex;


void main()
{
	gl_Position = uModelViewProjectionMatrix * position;
	v_color = color;
	v_texCoord = texCoord;
	v_texIndex = texIndex;
};