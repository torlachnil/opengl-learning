#version 430 core

layout(location = 0) in vec4 position;

uniform mat4 uModelViewProjectionMatrix;
uniform float uAngle = 0;

mat4 xyRotation(float angle)
{
	mat4 outMatrix = mat4( cos(angle), sin(angle), 0, 0,
						  -sin(angle), cos(angle), 0, 0,
						   0,		   0,		   1, 0,
						   0 ,		   0,   	   0, 1);
	return outMatrix;
};

void main()
{
	gl_Position = uModelViewProjectionMatrix * xyRotation(uAngle) * position;
};