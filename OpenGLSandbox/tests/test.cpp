#include "test.h"

#include <imgui/imgui.h>
#include <imgui/imgui_impl_glfw.h>
#include <imgui/imgui_impl_opengl3.h>



test::testMenu::testMenu(test*& testMenuPointer, GLFWwindow* window)
   :mCurrentTest(testMenuPointer), mWindow(window),
    mWidth(1280), mHeight(960)
{
}

void test::testMenu::onSetup() {
   glfwSetWindowSize(mWindow, mWidth, mHeight);
}

void test::testMenu::onRender()
{
   mRenderer.clear();
}

void test::testMenu::onImguiRender()
{
   for (auto& test : mTests)
   {
      if (ImGui::Button(test.first.c_str()))
      {
         mCurrentTest = test.second();
         mCurrentTest->onSetup(mWindow);
      }

   }
}

