#include "testBatchRendering.h"
#include <imgui/imgui.h>
#include <imgui/imgui_impl_glfw.h>
#include <imgui/imgui_impl_opengl3.h>

test::testBatchRendering::testBatchRendering()
   :mVa(), mVb(nullptr, NULL),
   mS("./shaders/vertColor.vert", "./shaders/vertColor.frag"),
   mIb(nullptr, NULL),
   mRenderer(),  //position (2f) //Color (rgba)
   mVertexBuffer{ -1.05f, -0.4f, 0.1f, 0.3f, 1.0f, 1.0f,
                  -0.25f, -0.4f, 0.1f, 0.3f, 1.0f, 1.0f,
                  -0.25f,  0.4f, 0.1f, 0.3f, 1.0f, 1.0f,
                  -1.05f,  0.4f, 0.1f, 0.3f, 1.0f, 1.0f,

                   0.25f, -0.4f, 1.0f, 0.94f, 0.24f, 1.0f,
                   1.05f, -0.4f, 1.0f, 0.94f, 0.24f, 1.0f,
                   1.05f,  0.4f, 1.0f, 0.94f, 0.24f, 1.0f,
                   0.25f,  0.4f, 1.0f, 0.94f, 0.24f, 1.0f },
   mIndexBuffer{ 0, 1, 2,
                 0, 2, 3,

                 4, 5, 6,
                 4, 6, 7},
   mVbl(),
   mProj(glm::ortho(-1.3333f, 1.3333f, -1.0f, 1.0f, -1.0f, 1.0f))
{
   mS.setUniformMat4f("uModelViewProjectionMatrix", mProj);
   mVa.bind();
   mVb.replaceData(&mVertexBuffer[0], sizeof(mVertexBuffer));
   mIb.replaceData(&mIndexBuffer[0], sizeof(mIndexBuffer)/sizeof(mIndexBuffer[0]));

   mVbl.push<float>(2, 0); //position
   mVbl.push<float>(4, 0); //color
   mVa.addBuffer(mVb, mVbl);
}

void test::testBatchRendering::onSetup(GLFWwindow* window)
{
   glfwSetWindowSize(window, 1440, 1080);
   glViewport(0, 0, 1440, 1080);
}

void test::testBatchRendering::onUpdate(float deltaTime)
{
   mVb.replaceData(&mVertexBuffer[0], sizeof(mVertexBuffer));
   mIb.replaceData(&mIndexBuffer[0], sizeof(mIndexBuffer) / sizeof(mIndexBuffer[0]));
   mVa.addBuffer(mVb, mVbl);
}

void test::testBatchRendering::onRender()
{
   mRenderer.clear();
   mRenderer.draw(mVa, mIb, mS);
}

void test::testBatchRendering::onImguiRender()
{
   ImGui::Text("Left square");
   ImGui::SliderFloat3("l-SW", &mVertexBuffer[2], 0.0f, 1.0f);
   ImGui::SliderFloat3("l-SE", &mVertexBuffer[8], 0.0f, 1.0f);
   ImGui::SliderFloat3("l-NE", &mVertexBuffer[14], 0.0f, 1.0f);
   ImGui::SliderFloat3("l-NW", &mVertexBuffer[20], 0.0f, 1.0f);
   ImGui::Text("Right square");
   ImGui::SliderFloat3("r-SW", &mVertexBuffer[26], 0.0f, 1.0f);
   ImGui::SliderFloat3("r-SE", &mVertexBuffer[32], 0.0f, 1.0f);
   ImGui::SliderFloat3("r-NE", &mVertexBuffer[38], 0.0f, 1.0f);
   ImGui::SliderFloat3("r-NW", &mVertexBuffer[44], 0.0f, 1.0f);
}
