#pragma once
#include <functional>
#include <string>
#include <vector>
#include <iostream>
#include <renderer.h>
namespace test {
   class test
   {
   public:
      test() {};
      virtual ~test() {};
      // Set down any global changes made to the openGL context window
      virtual void onSetup(GLFWwindow* window) = 0;
      // Update state of vertex/index buffers.
      virtual void onUpdate(float deltaTime) {}
      // Feed buffers to renderers
      virtual void onRender() {}
      // Imgui logic + rendering sohuld go here.
      virtual void onImguiRender() {}
      // Tear down any global changes made to the openGL context window
      virtual void onTearDown() = 0;
   };

   class testMenu : public test
   {
   public:
      testMenu(test*& testMenuPointer, GLFWwindow* window);

      void onSetup();
      void onSetup(GLFWwindow* window) override {}
      //testMenu is different from other tests, and ignores the interface.
      void onRender() override;
      void onImguiRender() override;
      void onTearDown() override {};
      template <typename T>
      void registerTest(std::string name)
      {
         std::cout << "Creating testcase: " << name << std::endl;
         mTests.push_back(std::make_pair(name, []() { return new T(); }));
      }

   private:
      test*& mCurrentTest;
      GLFWwindow* mWindow;
      int mWidth;
      int mHeight;
      std::vector<std::pair<std::string, std::function<test * ()>>> mTests;
      renderer mRenderer;
   };
}