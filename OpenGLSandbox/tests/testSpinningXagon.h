#pragma once
#include "test.h"
#include <math.h>

#include <renderer.h>
#include <vertexBuffer.h>
#include <indexbuffer.h>
#include <vertexArray.h>
#include <vertexBufferLayout.h>
#include <Shader.h>
#include <texture.h>


#define PI 3.1416f //overshoot to avoid missing slices =]

namespace test {


class testSpinningXagon : public test
{
public:
   testSpinningXagon();
   ~testSpinningXagon();

   void onSetup(GLFWwindow* window) override;
   void onUpdate(float deltaTime) override;
   void onRender() override;
   void onImguiRender() override;
   void onTearDown() override {};

private:
   void updateShape(unsigned int const& newNCorners);

   unsigned int mNCorners;
   unsigned int mNCornersUpdateable;
   float mTime;
   float mAngle;
   vertexArray mVa;
   vertexBuffer mVb;
   shader mS;
   indexBuffer mIb;
   renderer mRenderer;
   float mSpeed;
   std::vector<float> mPositionVector;
   std::vector<unsigned int> mVertexIndices;
};


}