#include "testBatchRenderTextures.h"

test::testBatchRenderTextures::testBatchRenderTextures()
   :mVa(), mVb(nullptr, NULL),
   mS("./shaders/texBatch.vert", "./shaders/texBatch.frag"),
   mIb(nullptr, NULL),
   mRenderer(),//position (2f)  |Color (rgba)           |Tex coords |texture index
   mVertexBuffer{ -1.05f, -0.4f, 0.1f, 0.3f, 1.0f, 1.0f, 0.0f, 0.0f, 0.0f,
                  -0.25f, -0.4f, 0.1f, 0.3f, 1.0f, 1.0f, 1.0f, 0.0f, 0.0f,
                  -0.25f,  0.4f, 0.1f, 0.3f, 1.0f, 1.0f, 1.0f, 1.0f, 0.0f,
                  -1.05f,  0.4f, 0.1f, 0.3f, 1.0f, 1.0f, 0.0f, 1.0f, 0.0f,

                   0.25f, -0.4f, 1.0f, 0.94f, 0.24f, 1.0f, 0.0f, 0.0f, 1.0f,
                   1.05f, -0.4f, 1.0f, 0.94f, 0.24f, 1.0f, 1.0f, 0.0f, 1.0f,
                   1.05f,  0.4f, 1.0f, 0.94f, 0.24f, 1.0f, 1.0f, 1.0f, 1.0f,
                   0.25f,  0.4f, 1.0f, 0.94f, 0.24f, 1.0f, 0.0f, 1.0f, 1.0f},
   mIndexBuffer{ 0, 1, 2,
                 0, 2, 3,

                 4, 5, 6,
                 4, 6, 7},
   mText1("pictures/LorentzianBCFTSpacetime.png"),
   mText2("pictures/Cheesecake.jpg"),
   mProj(glm::ortho(-1.3333f, 1.3333f, -1.0f, 1.0f, -1.0f, 1.0f))
{
   mS.setUniformMat4f("uModelViewProjectionMatrix", mProj);
   mVa.bind();
   mVb.replaceData(&mVertexBuffer[0], sizeof(mVertexBuffer));
   mIb.replaceData(&mIndexBuffer[0], sizeof(mIndexBuffer)/sizeof(mIndexBuffer[0]));
   vertexBufferLayout vbl;
   vbl.push<float>(2, 0); //position
   vbl.push<float>(4, 0); //color
   vbl.push<float>(2, 0); //texture coords
   vbl.push<float>(1, 0); //texture index
   mVa.addBuffer(mVb, vbl);
   int textureSlots[2] = { 0, 1 };
   mText1.bind(textureSlots[0]);
   mText2.bind(textureSlots[1]);
   mS.setUniform1iv("uTextures", sizeof(textureSlots), &textureSlots[0]);
}

void test::testBatchRenderTextures::onSetup(GLFWwindow* window)
{
   glfwSetWindowSize(window, 1440, 1080);
   glViewport(0, 0, 1440, 1080);
}

void test::testBatchRenderTextures::onUpdate(float deltaTime)
{
}

void test::testBatchRenderTextures::onRender()
{
   mRenderer.clear();
   mRenderer.draw(mVa, mIb, mS);
}

void test::testBatchRenderTextures::onImguiRender()
{
}
