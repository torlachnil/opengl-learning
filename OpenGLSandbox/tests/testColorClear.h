#pragma once

#include "test.h"
#include "renderer.h"

namespace test {

   class testClearColor : public test
   {
   public:
      testClearColor();
      ~testClearColor();

      void onSetup(GLFWwindow* window) override;
      void onUpdate(float deltaTime) override;
      void onRender() override;
      void onImguiRender() override;
      void onTearDown() override {};
   private:
      float mClearColor[4];
      renderer mRenderer;
   };
}