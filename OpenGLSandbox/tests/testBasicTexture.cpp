#include "testBasicTexture.h"
#include <imgui/imgui.h>
#include <imgui/imgui_impl_glfw.h>
#include <imgui/imgui_impl_opengl3.h>

test::testBasicTexture::testBasicTexture()
   : mVb(nullptr, 0),
   mS("./shaders/basic.vert", "./shaders/basic.frag"),
   mIb(nullptr, 0),
   mBCFTtexture("pictures/LorentzianBCFTSpacetime.png"),
   mVertexBuffer{-0.65f, -0.5f, 0.0f, 0.0f,
                  0.65f, -0.5f, 1.0f, 0.0f,
                  0.65f,  0.5f, 1.0f, 1.0f,
                 -0.65f,  0.5f, 0.0f, 1.0f},
   mIndices{ 0, 1, 2,
             0, 2, 3 },
   mModel(1.0f),
   mView(1.0f),
   mProj(glm::ortho(-1.3333f, 1.333f, -1.0f, 1.0f, -1.0f, 1.0f)),
   mPosition1(0.0f, 0.0f, 0.0f),
   mPosition2(-1.0f, 0.0f, 0.0f)
{
   mVb.replaceData(&mVertexBuffer[0], sizeof(mVertexBuffer));
   mIb.replaceData(&mIndices[0], sizeof(mIndices) / sizeof(mIndices[0]));
   vertexBufferLayout vbl;
   vbl.push<float>(2, GL_FALSE); // Vertex positions
   vbl.push<float>(2, GL_FALSE); // Testure coordinates of vertices
   mVa.addBuffer(mVb, vbl);

   mBCFTtexture.bind(0);
   mS.setUniform1i("uTexture", 0);
   glm::mat4 MVP = mProj * mView * mModel;
   mS.setUniformMat4f("uModelViewProjectionMatrix", MVP);
}

void test::testBasicTexture::onSetup(GLFWwindow* window)
{
   glfwSetWindowSize(window, 1280, 960);
   glViewport(0, 0, 1280, 960);
}



void test::testBasicTexture::onUpdate(float deltaTime)
{
}

void test::testBasicTexture::onRender()
{
   mRenderer.clear();
   mS.setUniform1i("uTexture", 0);
   drawAt(mPosition1);
   drawAt(mPosition2);
}

void test::testBasicTexture::onImguiRender()
{
   ImGui::SliderFloat2("Position 1", &mPosition1[0], -1.333f, 1.333f);
   ImGui::SliderFloat2("Position 2", &mPosition2[0], -1.333f, 1.333f);
}

void test::testBasicTexture::drawAt(glm::vec3 position)
{
   glm::mat4 model = glm::translate(mModel, position);
   glm::mat4 MVP = mProj * mView * model;
   mS.setUniformMat4f("uModelViewProjectionMatrix", MVP);
   mRenderer.draw(mVa, mIb, mS);
}
