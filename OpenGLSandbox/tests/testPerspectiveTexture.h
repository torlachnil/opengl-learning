#pragma once
#include "test.h"

#include <renderer.h>
#include <vertexBuffer.h>
#include <indexbuffer.h>
#include <vertexArray.h>
#include <vertexBufferLayout.h>
#include <Shader.h>
#include <texture.h>

#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>

namespace test
{

   class testPerspectiveTexture : public test
   {
   public:
      testPerspectiveTexture();

      void onSetup(GLFWwindow* window) override;
      void onUpdate(float deltaTime) override;
      void onRender() override;
      void onImguiRender() override;
      void onTearDown() override;

   private:
      vertexArray mVa;
      vertexBuffer mVb;
      shader mS;
      indexBuffer mIb;
      renderer mRenderer;
      texture mBCFTtexture;
      float mVertexBuffer[16];
      unsigned int mIndices [6];
      glm::mat4 mModel;
      glm::mat4 mView;
      glm::mat4 mProj;
      glm::vec3 mPosition1;
      glm::vec3 mPosition2;
      void drawAt(glm::vec3 position);
   };

}