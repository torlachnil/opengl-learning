#include "testPerspectiveTexture.h"
#include <imgui/imgui.h>
#include <imgui/imgui_impl_glfw.h>
#include <imgui/imgui_impl_opengl3.h>

test::testPerspectiveTexture::testPerspectiveTexture()
   : mVb(nullptr, 0),
   mS("./shaders/basic.vert", "./shaders/basic.frag"),
   mIb(nullptr, 0),
   mBCFTtexture("pictures/LorentzianBCFTSpacetime.png"),
   mVertexBuffer{-0.65f, -0.5f, 0.0f, 0.0f,
                  0.65f, -0.5f, 1.0f, 0.0f,
                  0.65f,  0.5f, 1.0f, 1.0f,
                 -0.65f,  0.5f, 0.0f, 1.0f},
   mIndices{ 0, 1, 2,
             0, 2, 3 },
   mModel(1.0f),
   mView(1.0f),
   mProj(glm::perspective(120.0f, 1.3333f, 0.01f, 150.0f)),
   mPosition1(0.0f, 0.0f, -4.0f),
   mPosition2(-1.0f, 0.0f, -4.0f)
{
   mVb.replaceData(&mVertexBuffer[0], sizeof(mVertexBuffer));
   mIb.replaceData(&mIndices[0], sizeof(mIndices) / sizeof(mIndices[0]));
   vertexBufferLayout vbl;
   vbl.push<float>(2, GL_FALSE); // Vertex positions
   vbl.push<float>(2, GL_FALSE); // Testure coordinates of vertices
   mVa.addBuffer(mVb, vbl);

   mBCFTtexture.bind(0);
   mS.setUniform1i("uTexture", 0);
   glm::mat4 MVP = mProj * mView * mModel;
   mS.setUniformMat4f("uModelViewProjectionMatrix", MVP);
}


void test::testPerspectiveTexture::onSetup(GLFWwindow* window)
{
   glfwSetWindowSize(window, 1440, 1080);
   glViewport(0, 0, 1440, 1080);
   glEnable(GL_DEPTH_TEST);
   glClearDepth(1.0f);
}

void test::testPerspectiveTexture::onUpdate(float deltaTime)
{
}

void test::testPerspectiveTexture::onRender()
{
   mRenderer.clear();
   mS.setUniform1i("uTexture", 0);
   drawAt(mPosition1);
   drawAt(mPosition2);

}

void test::testPerspectiveTexture::onImguiRender()
{
   ImGui::SliderFloat2("Position 1", &mPosition1[0], -1.333f, 1.333f);
   ImGui::SliderFloat2("Position 2", &mPosition2[0], -1.333f, 1.333f);
   ImGui::SliderFloat("Depth 1", &mPosition1[2], -10.0f, 0.0f);
   ImGui::SliderFloat("Depth 2", &mPosition2[2], -10.0f, 0.0f);
}

void test::testPerspectiveTexture::onTearDown()
{
   glDisable(GL_DEPTH_TEST);
}

void test::testPerspectiveTexture::drawAt(glm::vec3 position)
{
   glm::mat4 model = glm::translate(mModel, position);
   glm::mat4 MVP = mProj * mView * model;
   mS.setUniformMat4f("uModelViewProjectionMatrix", MVP);
   mRenderer.draw(mVa, mIb, mS);
}
