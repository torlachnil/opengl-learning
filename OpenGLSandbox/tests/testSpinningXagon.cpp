#include "testSpinningXagon.h"

#include <imgui/imgui.h>
#include <imgui/imgui_impl_glfw.h>
#include <imgui/imgui_impl_opengl3.h>

#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>

test::testSpinningXagon::testSpinningXagon()
   : mNCorners(4),
   mNCornersUpdateable(4),
   mTime(0.0f), mAngle(0.0f),
   mVb(&mNCorners, 1),
   mS("./shaders/xagon.vert", "./shaders/xagon.frag"),
   mIb((unsigned int*)&mNCorners, 1U), mSpeed(1.0f)
{
   glm::mat4 proj = glm::ortho(-1.3333f, 1.3333f, -1.0f, 1.0f, -1.0f, 1.0f);
   mS.setUniformMat4f("uModelViewProjectionMatrix", proj);
   mS.setUniform4f("uColor", 1.0f, 1.0f, 1.0f, 1.0f);
   updateShape(mNCorners);
}

test::testSpinningXagon::~testSpinningXagon()
{
}

void test::testSpinningXagon::onSetup(GLFWwindow* window)
{
   glfwSetWindowSize(window, 1440, 1080);
   glViewport(0, 0, 1440, 1080);
}

void test::testSpinningXagon::onUpdate(float deltaTime)
{
   mTime += deltaTime;
   mAngle += deltaTime * mSpeed;
   if (mNCornersUpdateable > 0 && mNCornersUpdateable < 100000 &&
      mNCornersUpdateable != mNCorners)
   {
      updateShape(mNCornersUpdateable);
      mNCorners = mNCornersUpdateable;
   }
}

void test::testSpinningXagon::onRender()
{
   mRenderer.clear();

   mS.bind();
   mS.setUniform4f("uColor", 1.0f * sinf(mTime), 0.5f, 0.5f, 1.0f);
   mS.setUniform1f("uAngle", fmod((mAngle / 20.0f), 2.0f * PI));

   mRenderer.draw(mVa, mIb, mS);
}

void test::testSpinningXagon::onImguiRender()
{
   ImGui::SliderFloat("Rotational speed factor", &mSpeed, 10.0f, -10.0f);
   ImGui::InputInt("Number of corners", (int*) &mNCornersUpdateable, 1);
   ImGui::Text("Application average %.3f ms/frame (%.1f FPS)",
      1000.0f / ImGui::GetIO().Framerate, ImGui::GetIO().Framerate);
}

void test::testSpinningXagon::updateShape(unsigned const int& newNCorners)
{
   mPositionVector.clear();
   mVertexIndices.clear();
   mPositionVector.push_back(0.0f);
   mPositionVector.push_back(0.0f);
   float angle;
   for (unsigned int corner = 0; corner <= newNCorners; corner++) {
      angle = corner * (2 * PI) / newNCorners;
      mPositionVector.push_back(0.7f * cosf(angle));
      mPositionVector.push_back(0.7f * sinf(angle));
      if (corner >= 1)
      {
         mVertexIndices.push_back(0);
         mVertexIndices.push_back(corner);
         mVertexIndices.push_back(corner + 1);
      }
   }
   mVb.replaceData(&mPositionVector[0], sizeof(float) * mPositionVector.size());
   mIb.replaceData(&mVertexIndices[0], mVertexIndices.size());
   mIb.bind();

   vertexBufferLayout vbl;
   vbl.push<float>(2, GL_FALSE);
   mVa.addBuffer(mVb, vbl);
}
