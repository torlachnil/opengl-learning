#pragma once
#include "test.h"

#include <renderer.h>
#include <vertexBuffer.h>
#include <indexbuffer.h>
#include <vertexArray.h>
#include <vertexBufferLayout.h>
#include <Shader.h>
#include <texture.h>

#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>

namespace test
{
   class testBatchRendering : public test
   {
   public:
      testBatchRendering();
      void onSetup(GLFWwindow* window) override;
      void onUpdate(float deltaTime) override;
      void onRender() override;
      void onImguiRender() override;
      void onTearDown() override {};

   private:
      vertexArray mVa;
      vertexBuffer mVb;
      shader mS;
      indexBuffer mIb;
      renderer mRenderer;
      vertexBufferLayout mVbl;
      float mVertexBuffer[48];
      unsigned int mIndexBuffer[12];
      //glm::mat4 mModel;
      //glm::mat4 mView;
      glm::mat4 mProj;
      //glm::vec3 mPosition1;
      //glm::vec3 mPosition2;
   };
}