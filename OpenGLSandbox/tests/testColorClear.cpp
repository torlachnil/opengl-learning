#include "testColorClear.h"
#include <GL/glew.h>
#include <GLFW/glfw3.h>
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <renderer.h>

#include <imgui/imgui.h>
#include <imgui/imgui_impl_glfw.h>
#include <imgui/imgui_impl_opengl3.h>

#include <assert.h>
#include <vertexBuffer.h>
#include <indexbuffer.h>
#include <vertexArray.h>
#include <vertexBufferLayout.h>
#include <Shader.h>
#include <util.h>
#include <texture.h>

test::testClearColor::testClearColor()
   : mClearColor{ 0.5f, 0.5f, 0.5f, 1.0f }
{

}

test::testClearColor::~testClearColor()
{
}

void test::testClearColor::onSetup(GLFWwindow* window)
{
   glfwSetWindowSize(window, 800, 800);
   glViewport(0, 0, 800, 800);
}

void test::testClearColor::onUpdate(float deltaTime)
{
}

void test::testClearColor::onRender()
{
   mRenderer.clear(mClearColor[0], mClearColor[1],
                mClearColor[2], mClearColor[3]);
}

void test::testClearColor::onImguiRender()
{

   ImGui::ColorEdit4("clear color", &mClearColor[0]);
   ImGui::Text("Application average %.3f ms/frame (%.1f FPS)",
      1000.0f / ImGui::GetIO().Framerate, ImGui::GetIO().Framerate);

}
