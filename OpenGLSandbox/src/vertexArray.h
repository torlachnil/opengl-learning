#pragma once
#include "vertexBufferLayout.h"
#include "vertexBuffer.h"

class vertexArray
{
private:
   unsigned int rendererID;
public:
   vertexArray();
   ~vertexArray();

   void addBuffer(const vertexBuffer& vb,
                  const vertexBufferLayout& layout) const;

   void bind() const;
   void unbind() const;
};