#include "indexBuffer.h"
#include "renderer.h"

indexBuffer::indexBuffer(const unsigned int* data, unsigned int count)
   : indexCount(count)
{

   glGenBuffers(1, &rendererId);
   glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, rendererId);
   glBufferData(GL_ELEMENT_ARRAY_BUFFER, count * sizeof(unsigned int),
                data, GL_STATIC_DRAW);
}

indexBuffer::~indexBuffer()
{
   glDeleteBuffers(1, &rendererId);
}

void indexBuffer::replaceData(const unsigned int* data, unsigned int count)
{
   indexCount = count;
   glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, rendererId);
   glBufferData(GL_ELEMENT_ARRAY_BUFFER, count * sizeof(unsigned int),
      data, GL_STATIC_DRAW);
}

void indexBuffer::bind() const
{
   glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, rendererId);
}

void indexBuffer::unbind() const
{
   glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);
}
