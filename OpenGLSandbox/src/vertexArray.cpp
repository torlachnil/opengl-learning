#include "vertexArray.h"

vertexArray::vertexArray()
{
   glGenVertexArrays(1, &rendererID);
   glBindVertexArray(rendererID);
}

vertexArray::~vertexArray()
{
   glDeleteVertexArrays(1, &rendererID);
}

void vertexArray::addBuffer(const vertexBuffer& vb,
                            const vertexBufferLayout& layout) const
{
   vb.bind();
   const auto& elements = layout.getElements();
   unsigned int offset = 0;
   for (unsigned int i = 0; i < elements.size(); i++)
   {
      const auto& element = elements[i];
      glEnableVertexAttribArray(i);
      glVertexAttribPointer(i, element.count, element.type,
                            element.normalized, layout.getStride(),
                            (const void*) offset);
      offset += vertexBufferElement::getSizeOfType(element.type)
                  * element.count;
   }
}

void vertexArray::bind() const
{
   glBindVertexArray(rendererID);
}

void vertexArray::unbind() const
{
   glBindVertexArray(0);
}
