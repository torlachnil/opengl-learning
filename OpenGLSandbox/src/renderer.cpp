#include "renderer.h"
#include <fstream>
#include <iostream>




void renderer::clear(float r, float g,
                     float b, float a) const
{
   glClearColor(r, g, b, a);
   glClearDepth(1.0f);
   glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
}

void renderer::draw(const vertexArray& va, const indexBuffer& ib,
                     const shader& shader) const
{
   shader.bind();
   va.bind();
   ib.bind();
   glDrawElements(GL_TRIANGLES, ib.getCount() * sizeof(GLuint),
                  GL_UNSIGNED_INT, NULL);
}
