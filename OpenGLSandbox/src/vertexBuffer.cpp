#include "vertexBuffer.h"
#include "renderer.h"

vertexBuffer::vertexBuffer(const void* data, unsigned int size)
{
   glGenBuffers(1, &rendererId);
   glBindBuffer(GL_ARRAY_BUFFER, rendererId);
   glBufferData(GL_ARRAY_BUFFER, size, data, GL_STATIC_DRAW);
}


vertexBuffer::~vertexBuffer()
{
   glDeleteBuffers(1, &rendererId);
}



void vertexBuffer::replaceData(const void* data, unsigned int size) const
{
   glBindBuffer(GL_ARRAY_BUFFER, rendererId);
   glBufferData(GL_ARRAY_BUFFER, size, data, GL_STATIC_DRAW);
}

void vertexBuffer::bind() const
{
   glBindBuffer(GL_ARRAY_BUFFER, rendererId);
}

void vertexBuffer::unbind() const
{
   glBindBuffer(GL_ARRAY_BUFFER, 0);
}
