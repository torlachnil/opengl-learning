#include <GL/glew.h>
#include <GLFW/glfw3.h>
#include <string>
#include <fstream>
#include <math.h>
#include <iostream>
#include <time.h>
#include <memory>
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>

#include <imgui/imgui.h>
#include <imgui/imgui_impl_glfw.h>
#include <imgui/imgui_impl_opengl3.h>

#include <assert.h>
#include "renderer.h"
#include "vertexBuffer.h"
#include "indexbuffer.h"
#include "vertexArray.h"
#include "vertexBufferLayout.h"
#include "Shader.h"
#include "util.h"
#include "texture.h"

#include "../tests/testColorClear.h"
#include "../tests/testSpinningXagon.h"
#include "../tests/testBasicTexture.h"
#include "../tests/testPerspectiveTexture.h"
#include "../tests/testBatchRendering.h"
#include "../tests/testBatchRenderTextures.h"

// #define NDEBUG if you want to disable asserts for some absurd reason.

int main() {
   GLFWwindow* window;


   /*initialize library*/
   if (!glfwInit()) {
      return -1;
   }

   //glfwWindowHint(GLFW_OPENGL_DEBUG_CONTEXT, GL_TRUE);
   //glfwWindowHint(GLFW_OPENGL_FORWARD_COMPAT, GL_TRUE);
   glfwWindowHint(GLFW_VERSION_MAJOR, 4);
   glfwWindowHint(GLFW_VERSION_MINOR, 5);
   //glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_COMPAT_PROFILE);


   /* Create a windowed mode window and it's OpenGL context */
   window = glfwCreateWindow(1280, 960, "Application", NULL, NULL);
   if (!window) {
      glfwTerminate();
      return -1;
   }
   ImGui::CreateContext();

   ImGui_ImplGlfw_InitForOpenGL(window, true);
   ImGui_ImplOpenGL3_Init("#version 430");
   /* Make the window's context current */
   glfwMakeContextCurrent(window);
   bool errorFlag = glewInit();
   if (errorFlag != GLEW_OK) {
      std::cout << "GLEW NOT OK!" << std::endl;
   }
   glfwSwapInterval(1);

   std::cout << glGetString(GL_VERSION) << std::endl;

   /* Enable debug output */
   glEnable(GL_ERROR_REGAL);
   glDebugMessageCallback(MessageCallback, nullptr);
   { // Scope to kill stack allocated buffers before openGL context is removed
      /* Initialize & populate Vertex buffer */

      glEnable(GL_BLEND);
      glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

      /* While loop until window is closed */
      float timeStep = 1 / 30.0f;

      test::test* currentTest;
      test::testMenu* menu = new test::testMenu(currentTest, window);
      menu->registerTest<test::testClearColor>("Clear color test");
      menu->registerTest<test::testSpinningXagon>("Spinning polygon test");
      menu->registerTest<test::testBasicTexture>("2d two textures test");
      menu->registerTest<test::testPerspectiveTexture>("Perspective proj test");
      menu->registerTest<test::testBatchRendering>("Batch rendering colors");
      menu->registerTest<test::testBatchRenderTextures>("Batch rendering textures");
      // Change this to specific test or an arg
      currentTest = menu;


      while (!glfwWindowShouldClose(window))
      {
         /* Poll for and process events */
         glfwPollEvents();
         if (currentTest)
         {
            currentTest->onUpdate(timeStep);

            /* Render what is in the buffers */
            currentTest->onRender();

            /* Render imgui stuff */
            ImGui_ImplOpenGL3_NewFrame();
            ImGui_ImplGlfw_NewFrame();
            ImGui::NewFrame();
            ImGui::Begin("Test menu");
            if (currentTest != menu && ImGui::Button("<--- return"))
            {
               currentTest->onTearDown();
               delete currentTest;
               currentTest = menu;
               menu->onSetup();
            }
            currentTest->onImguiRender();

            ImGui::End();
            ImGui::Render();
            ImGui_ImplOpenGL3_RenderDrawData(ImGui::GetDrawData());
         }
         /* Swap front and back buffers */
         glfwSwapBuffers(window);
      }
      if (currentTest && currentTest != menu)
      {
         delete currentTest;
      }
      delete menu;
   }

   ImGui_ImplOpenGL3_Shutdown();
   ImGui_ImplGlfw_Shutdown();
   ImGui::DestroyContext();

   glfwTerminate();
   return 0;
}

