#pragma once

class vertexBuffer
{
private:
   unsigned int rendererId;

public:
   vertexBuffer(const void* data, unsigned int size);
   ~vertexBuffer();

   void replaceData(const void* data, unsigned int size) const;
   void bind() const;
   void unbind() const;
};