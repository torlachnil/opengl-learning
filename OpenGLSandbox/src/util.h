#pragma once
#include <gl/glew.h>

/* GL callback function to print debugging infromation. Taken from
* https://www.khronos.org/opengl/wiki/Example/OpenGL_Error_Testing_with_Message_Callbacks
*/
void (GLAPIENTRY MessageCallback)(GLenum source,
   GLenum type,
   GLuint id,
   GLenum severity,
   GLsizei length,
   const GLchar* message,
   const void* userParam);

static void GLClearError();

static void GLCheckError();