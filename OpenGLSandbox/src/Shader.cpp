#include "Shader.h"
#include <iostream>
#include <fstream>
#include <GL/glew.h>
#include <assert.h>



shader::shader(const std::string& vertexShaderPath,
               const std::string& fragmentShaderPath)
   : vertexPath(vertexShaderPath), fragmentPath(fragmentShaderPath),
     rendererID(0)
{
   std::string vertexSource = makeSource(vertexShaderPath);
   std::string fragmentSource = makeSource(fragmentShaderPath);
   rendererID = createShader(vertexSource, fragmentSource);
   glUseProgram(rendererID);
}

shader::~shader()
{
   glDeleteProgram(rendererID);
}

void shader::bind() const
{
   glUseProgram(rendererID);
}

void shader::unbind() const
{
   glUseProgram(NULL);
}

void shader::setUniform1i(const std::string name, int v0)
{
   unsigned int location = getUniformLocation(name);
   glUniform1i(location, v0);
}

void shader::setUniform1iv(const std::string name, int count,
                           int* textureSlots)
{
   unsigned int location = getUniformLocation(name);
   glUniform1iv(location, count, textureSlots);
}

void shader::setUniform1f(const std::string name, float v0)
{
   unsigned int location = getUniformLocation(name);
   glUniform1f(location, v0);
}

void shader::setUniform4f(const std::string name, float r, float g, float b, float alpha)
{
   unsigned int location = getUniformLocation(name);
   glUniform4f(location, r, g, b, alpha);
}

void shader::setUniformMat4f(const std::string name, const glm::mat4& matrix)
{
   unsigned int location = getUniformLocation(name);
   glUniformMatrix4fv(location, 1, GL_FALSE, &matrix[0][0]);
}


unsigned int shader::getUniformLocation(const std::string name)
{
   if (uniformLocationCache.find(name) != uniformLocationCache.end())
   {
      // Keep a cache of already known locations because
      // glGetUniformLocation is so slow we want to avoid calling it a lot.
      return uniformLocationCache[name];
   }
   unsigned int location = glGetUniformLocation(rendererID, name.c_str());
   uniformLocationCache[name] = location;
   if (location == -1) {
      std::cout << "Warning, uniform " << name <<
         " doesn't exist!" << std::endl;
   }
   return location;
}

std::string shader::makeSource(std::string filepath)
{
   std::ifstream fileStream(filepath);
   std::string fileContent(std::istreambuf_iterator<char>{fileStream},
      std::istreambuf_iterator<char>{});
   return fileContent;
}

/*
IN: source              Source code for generic shader
IN: type                Type of shader (some #define from GLEW)
*/
unsigned int shader::compileShader(const std::string& source, unsigned int type)
{
   unsigned int id = glCreateShader(type);
   const char* src = source.c_str(); // or &source[0]
   glShaderSource(id, 1, &src, NULL);
   glCompileShader(id);

   int result;
   glGetShaderiv(id, GL_COMPILE_STATUS, &result);
   if (result == GL_FALSE)
   {
      int length;
      glGetShaderiv(id, GL_INFO_LOG_LENGTH, &length);
      // alloca is a tool for manually allocating on the stack so it gets cleaned on return
      char* message = (char*)alloca(length * sizeof(char));
      glGetShaderInfoLog(id, length, &length, message);
      std::cout << "Failed to compile " <<
         (type == GL_VERTEX_SHADER ? "vertex" : "fragment") << std::endl;
      std::cout << message << std::endl;
      glDeleteShader(id);
      return 0;
   }
   return id;
}

/*
IN: vertexShader         Source code for the vertex shader
IN: fragmentShader       Source code for the fragment shader
*/
unsigned int shader::createShader(const std::string& vertexShader,
   const std::string& fragmentShader)
{
   unsigned int program = glCreateProgram();
   unsigned int vs = compileShader(vertexShader, GL_VERTEX_SHADER);
   unsigned int fs = compileShader(fragmentShader, GL_FRAGMENT_SHADER);

   glAttachShader(program, vs);
   glAttachShader(program, fs);
   glLinkProgram(program);
   glValidateProgram(program);

   glDeleteShader(vs);
   glDeleteShader(fs);

   return program;
}