#pragma once

class indexBuffer
{
private:
   unsigned int rendererId;
   unsigned int indexCount;
public:
   indexBuffer(const unsigned int* data, unsigned int count);
   ~indexBuffer();

   void replaceData(const unsigned int* data, unsigned int count);
   void bind() const;
   void unbind() const;

   inline unsigned int getCount() const { return indexCount; };
};