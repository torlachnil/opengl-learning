#include <GL/glew.h>
#include <GLFW/glfw3.h>
#include <string>
#include <fstream>
#include <math.h>
#include <iostream>

#include <assert.h>
#include "renderer.h"
#include "vertexBuffer.h"
#include "indexbuffer.h"
#include "vertexArray.h"
#include "vertexBufferLayout.h"
#include "Shader.h"
#include "util.h"
#include "texture.h"

#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>


// #define NDEBUG if you want to disable asserts for some absurd reason.
#define PI 3.1416f //overshoot to avoid missing slices =]
#define NO_OF_CORNERS 8


int main() {
   GLFWwindow* window;


   /*initialize library*/
   if (!glfwInit()) {
      return -1;
   }

   //glfwWindowHint(GLFW_OPENGL_DEBUG_CONTEXT, GL_TRUE);
   //glfwWindowHint(GLFW_OPENGL_FORWARD_COMPAT, GL_TRUE);
   glfwWindowHint(GLFW_VERSION_MAJOR, 4);
   glfwWindowHint(GLFW_VERSION_MINOR, 5);
   //glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_COMPAT_PROFILE);


   /* Create a windowed mode window and it's OpenGL context */
   window = glfwCreateWindow(1280, 960, "Hello World", NULL, NULL);
   if (!window) {
      glfwTerminate();
      return -1;
   }

   /* Make the window's context current */
   glfwMakeContextCurrent(window);
   bool errorFlag = glewInit();
   if (errorFlag != GLEW_OK) {
      std::cout << "GLEW NOT OK!" << std::endl;
   }
   glfwSwapInterval(1);

   std::cout << glGetString(GL_VERSION) << std::endl;

   /* Enable debug output */
   glEnable(GL_DEBUG_OUTPUT);
   glDebugMessageCallback(MessageCallback, nullptr);
   { // Scope to kill stack allocated buffers before openGL context is removed
      /* Initialize & populate Vertex & index buffer date */
      unsigned int N = NO_OF_CORNERS;
      std::vector<float> positionVector;
      std::vector<unsigned int> vertexIndices;

      positionVector.push_back(0.0f);
      positionVector.push_back(0.0f);
      for (unsigned int corner = 0; corner <= N; corner++) {
         float angle = corner * (2 * PI) / N;
         positionVector.push_back(0.8f * cosf(angle));
         positionVector.push_back(0.8f * sinf(angle));
         if (corner >= 1)
         {
            vertexIndices.push_back(0);
            vertexIndices.push_back(corner);
            vertexIndices.push_back(corner + 1);
         }
      }


      vertexArray va;
      vertexBuffer vertices(&positionVector[0], sizeof(float) * positionVector.size());
      vertices.bind();

      vertexBufferLayout layout;
      layout.push<float>(2, GL_FALSE);
      va.addBuffer(vertices, layout);

      indexBuffer ib(&vertexIndices[0], vertexIndices.size());
      ib.bind();


      shader s("./shaders/xagon.vert", "./shaders/xagon.frag");
      s.bind();

      glm::mat4 proj = glm::ortho(-1.3333f, 1.3333f, -1.0f, 1.0f, -1.0f, 1.0f);
      s.setUniformMat4f("uModelViewProjectionMatrix", proj);
      s.setUniform4f("uColor", 0.0f, 0.0f, 0.0f, 0.0f);


      vertices.unbind();
      s.unbind();
      va.unbind();
      ib.unbind();
      //bCFTtexture.unbind();
      renderer mainRenderer;

      /* While loop until window is closed */
      float colorTimeStep = 1 / 30.0f;
      float time = 0;
      while (!glfwWindowShouldClose(window)) {
         mainRenderer.clear();

         s.bind();
         s.setUniform4f("uColor", 1.0f * sinf(time), 0.5f, 0.5f, 1);
         s.setUniform1f("uAngle", fmod((time / 20.0f), 2.0f * PI));
         mainRenderer.draw(va, ib, s);
         /* Swap front and back buffers */
         glfwSwapBuffers(window);

         /* Poll for and process events */
         glfwPollEvents();
         time += colorTimeStep;

         va.unbind();
      }
   }


   glfwTerminate();
   return 0;
}

