#pragma once
#include <vector>
#include <GL/glew.h>
#include <assert.h>

struct vertexBufferElement
{
   unsigned int count;
   unsigned int type;
   bool normalized;

   static unsigned int getSizeOfType(unsigned int type)
   {
      switch (type)
      {
      case GL_FLOAT:         return sizeof(GLfloat);
      case GL_UNSIGNED_INT:  return sizeof(GLuint);
      case GL_UNSIGNED_BYTE: return sizeof(GLubyte);
      }
      assert(false);
      return 0;
   }
};

class vertexBufferLayout
{
private:
   std::vector<vertexBufferElement> elements;
   unsigned int stride;
public:
   vertexBufferLayout()
      : stride(0) {}

   template<typename T>
   void push(unsigned int count, bool normalized)
   { // Don't compile if unsupported type is provided
      static_assert(false);
   }

   template<>
   void push<float>(unsigned int count, bool normalized)
   {
      elements.push_back({ count, GL_FLOAT, normalized });
      stride += vertexBufferElement::getSizeOfType(GL_FLOAT) * count;
   }

   template<>
   void push<int>(unsigned int count, bool normalized)
   {
      elements.push_back({ count, GL_UNSIGNED_INT, normalized });
      stride += vertexBufferElement::getSizeOfType(GL_UNSIGNED_INT) * count;
   }


   template<>
   void push<unsigned char>(unsigned int count, bool normalized)
   {
      elements.push_back({ count, GL_UNSIGNED_BYTE, normalized });
      stride += vertexBufferElement::getSizeOfType(GL_UNSIGNED_BYTE) * count;
   }

   inline std::vector<vertexBufferElement> getElements() const
   {
      return elements;
   }

   inline unsigned int getStride() const& { return stride; }
};