#pragma once
#include <string>

class texture
{
private:
   std::string filePath;
   int width;
   int height;
   unsigned char* localbuffer;
   int bitsPerPixel;
   unsigned int rendererId;
public:
   texture(const std::string path);
   ~texture();

   void bind(unsigned int slot = 0) const;
   void unbind() const;

   unsigned int getWidth() const { return width; }
   unsigned int getHeight() const { return height; }
};