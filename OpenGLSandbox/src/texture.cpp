#include "texture.h"
#include <gl/glew.h>
#include <stb_image.h>

texture::texture(const std::string path)
   :filePath(path), width(0), height(0),
    localbuffer(nullptr), bitsPerPixel(0)
{
   stbi_set_flip_vertically_on_load(1);
   localbuffer = stbi_load(path.c_str(),
                           &width, &height, &bitsPerPixel, 4);

   glGenTextures(1, &rendererId);
   glBindTexture(GL_TEXTURE_2D, rendererId);

   glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
   glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
   glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
   glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);

   glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA8, width, height,
                0, GL_RGBA, GL_UNSIGNED_BYTE, localbuffer);
   if (localbuffer)
   {
      stbi_image_free(localbuffer);
   }
}

texture::~texture()
{
   glDeleteTextures(1, &rendererId);
}

void texture::bind(unsigned int slot) const
{
   glActiveTexture(GL_TEXTURE0 + slot);
   glBindTexture(GL_TEXTURE_2D, rendererId);
}

void texture::unbind() const
{
}
