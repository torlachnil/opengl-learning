
#include "util.h"
#include <fstream>
#include <iostream>


/* GL callback function to print debugging infromation. Taken from
* https://www.khronos.org/opengl/wiki/Example/OpenGL_Error_Testing_with_Message_Callbacks
*/
void (GLAPIENTRY MessageCallback)(GLenum source,
   GLenum type,
   GLuint id,
   GLenum severity,
   GLsizei length,
   const GLchar* message,
   const void* userParam)
{
   fprintf(stderr, "GL CALLBACK: %s type = 0x%x, severity = 0x%x, message = %s\n",
      (type == GL_DEBUG_TYPE_ERROR ? "** GL ERROR **" : ""),
      type, severity, message);
}

static void GLClearError()
{
   while (glGetError() != GL_NO_ERROR);
}

static void GLCheckError()
{
   while (GLenum error = glGetError())
   {
      std::cout << "[OpenGL Error] : " << error << std::endl;
   }
}