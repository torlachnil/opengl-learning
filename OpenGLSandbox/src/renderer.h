#pragma once
#include <GL/glew.h>
#include <GLFW/glfw3.h>
#include "vertexArray.h"
#include "shader.h"
#include "indexBuffer.h"



class renderer
{
private:

public:
   void clear(float r = 0.0f, float g = 0.0f,
              float b = 0.0f, float a = 1.0f) const;
   void draw(const vertexArray& va, const indexBuffer& ib,
             const shader& shader) const;
};