#pragma once
#include <string>
#include <unordered_map>
#include <glm/glm.hpp>

class shader {
private:
   unsigned int rendererID;
   std::string vertexPath;
   std::string fragmentPath;
   unsigned int getUniformLocation(const std::string name);
   unsigned int compileShader(const std::string& source, unsigned int type);
   std::string makeSource(std::string filepath);
   unsigned int createShader(const std::string& vertexShader,
                             const std::string& fragmentShader);
   std::unordered_map<std::string, unsigned int> uniformLocationCache;
public:
   shader(const std::string& vertexShaderPath,
          const std::string& fragmentShaderPath);
   ~shader();

   void bind() const;
   void unbind() const;

   void setUniform1i(const std::string name, int v0);
   void setUniform1iv(const std::string name, int count,
      int* textureSlots);
   void setUniform1f(const std::string name, float v0);
   void setUniform4f(const std::string name,
                     float v0, float v1, float v3, float v4);
   void setUniformMat4f(const std::string name, const glm::mat4& matrix);
};